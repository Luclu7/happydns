---
kind: pipeline
type: docker
name: build-amd64

platform:
  os: linux
  arch: amd64

steps:
- name: frontend
  image: node:alpine
  commands:
  - apk --no-cache add python2 build-base
  - yarn config set network-timeout 100000
  - yarn --cwd htdocs install
  - yarn --cwd htdocs --offline build

- name: backend
  image: golang:alpine
  commands:
  - apk --no-cache add go-bindata
  - sed -i '/yarn --cwd htdocs --offline build/d' static.go
  - go generate -v
  - go build -v -o happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}

- name: vet
  image: golang:alpine
  commands:
  - apk --no-cache add build-base
  - go vet -v

- name: deploy
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_BRANCH//\//-}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - push

- name: deploy release
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_TAG}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - tag

- name: build macOS
  image: golang:alpine
  commands:
  - go build -v -o happydns-darwin-${DRONE_STAGE_ARCH}
  environment:
    GOOS: darwin
    GOARCH: amd64

- name: deploy macOS
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_BRANCH//\//-}
    source: happydns-darwin-${DRONE_STAGE_ARCH}
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - push

- name: deploy macOS release
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_TAG}
    source: happydns-darwin-${DRONE_STAGE_ARCH}
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - tag

- name: publish
  image: plugins/docker
  settings:
    repo: happydns/happydns
    auto_tag: true
    auto_tag_suffix: ${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    username:
      from_secret: docker_username
    password:
      from_secret: docker_password

---
kind: pipeline
type: docker
name: build-arm64

platform:
  os: linux
  arch: arm64

steps:
- name: frontend
  image: node:alpine
  commands:
  - apk --no-cache add python2 build-base
  - yarn config set network-timeout 100000
  - yarn --cwd htdocs install
  - yarn --cwd htdocs --offline build

- name: backend
  image: golang:alpine
  commands:
  - apk --no-cache add go-bindata
  - sed -i '/yarn --cwd htdocs --offline build/d' static.go
  - go generate -v
  - go build -v -o happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
  - ln happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH} happydns

- name: deploy
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_BRANCH//\//-}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - push

- name: deploy release
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_TAG}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - tag

- name: publish
  image: plugins/docker
  settings:
    repo: happydns/happydns
    auto_tag: true
    auto_tag_suffix: ${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    dockerfile: Dockerfile-builded
    username:
      from_secret: docker_username
    password:
      from_secret: docker_password

---
kind: pipeline
type: docker
name: build-arm

platform:
  os: linux
  arch: arm

steps:
- name: frontend
  image: node:alpine
  commands:
  - apk --no-cache add python2 build-base
  - yarn config set network-timeout 100000
  - yarn --cwd htdocs install
  - yarn --cwd htdocs --offline build

- name: backend armel
  image: golang:alpine
  commands:
  - apk --no-cache add go-bindata build-base
  - sed -i '/yarn --cwd htdocs --offline build/d' static.go
  - go generate -v
  - go build -v -o happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}el
  environment:
    GOARM: 5

- name: deploy armel
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_BRANCH//\//-}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}el
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - push

- name: deploy armel release
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_TAG}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}el
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - tag

- name: backend armhf
  image: golang:alpine
  commands:
  - apk --no-cache add build-base
  - go build -v -o happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}hf
  environment:
    GOARM: 6

- name: deploy armhf
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_BRANCH//\//-}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}hf
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - push

- name: deploy armhf release
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_TAG}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}hf
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - tag

- name: backend armv7
  image: golang:alpine
  commands:
  - apk --no-cache add build-base
  - go build -v -o happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}v7
  - ln happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}v7 happydns
  environment:
    GOARM: 7

- name: deploy armv7
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_BRANCH//\//-}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}v7
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - push

- name: deploy armv7 release
  image: appleboy/drone-scp
  settings:
    tar_tmp_path: /tmp/
    host: get.happydns.org
    target: /var/www/happydns.org/get/${DRONE_TAG}
    source: happydns-${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}v7
    username:
      from_secret: ssh_username
    key:
      from_secret: deploy_key
    port:
      from_secret: ssh_port
  when:
    event:
      - tag

- name: publish
  image: plugins/docker
  settings:
    repo: happydns/happydns
    auto_tag: true
    auto_tag_suffix: ${DRONE_STAGE_OS}-${DRONE_STAGE_ARCH}
    dockerfile: Dockerfile-builded
    username:
      from_secret: docker_username
    password:
      from_secret: docker_password

---
kind: pipeline
name: docker-manifest

steps:
- name: publish
  image: plugins/manifest
  settings:
    auto_tag: true
    ignore_missing: true
    spec: .drone-manifest.yml
    username:
      from_secret: docker_username
    password:
      from_secret: docker_password

trigger:
  event:
  - push
  - tag

depends_on:
- build-amd64
- build-arm64
