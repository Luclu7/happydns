{
    "account": {
        "ask-have": "Don't already have an account on our beautiful platform?",
        "delete": {
            "delete": "Delete my account",
            "deleted": "Account Deleted",
            "confirm": "If you want to delete your account and all data associated with it, press the button below:",
            "confirm-twice": "By confirming the deletion, you will permanently and irrevocably delete your account from our database and will loose your access to our easy management interface for your domains.",
            "confirm-password": "To ensure this is really you, please enter your password:",
            "consequence": "Your domains owned on others platforms will not be affected by the deletion, they'll continue to respond with the current dataset.",
            "remain-data": "For technical reason, your account will be deleted right after your validation, but some data from your account will persist until the next database clean up.",
            "success": "Your account have been successfully deleted. We hope to see you back soon."
        },
        "join": "Join now!",
        "ready-login": "Ready to login!",
        "signup": {
            "already": "Already a member?",
            "join-call": "Join our nice platform in less than 2 minutes!",
            "address-why": "You'll use your address to {identify} yourself on the platform, and it could be used to contact you for {security-operations}.",
            "identify": "identify",
            "security-operations": "security related operations",
            "receive-update": "Keep me informed of future big improvements",
            "signup": "Sign up!",
            "success": "Registration successfully performed!"
        },
        "see-again": "Happy to see you again!"
    },
    "common": {
        "add": "Add",
        "add-new-thing": "Add new {thing}",
        "cancel": "Cancel",
        "cancel-edit": "@:common.cancel edit",
        "continue": "Continue",
        "create-thing": "Create {thing}",
        "delete": "Delete",
        "domain": "Domain or subdomain",
        "edit": "Edit",
        "field": "Field",
        "go": "Go!",
        "name": "Name",
        "password": "Password",
        "rename": "Rename",
        "resolver": "Resolver",
        "run": "Run the request!",
        "spinning": "Spinning",
        "welcome": "Welcome to {0}!",
        "help": "Help!",
        "records": "no {type} record | {type} record | {type} records"
    },
    "domains": {
        "kind": "domain",
        "actions": {
            "reimport": "Re-import",
            "view": "View my zone",
            "propagate": "Publish my changes",
            "rollback": "Rollback to this version"
        },
        "alert": {
            "remove": "This action will permanently remove the domain {domain} from your managed domains. All history and abstracted zones will be discarded. This action will not delete or unregister your domain from your provider, nor alterate what is currently served. It will only affect what you see in happyDNS. Are you sure you want to continue?",
            "unable-retrieve": {
                "description": "Unfortunately, we were unable to retrieve information for the domain {domain}:",
                "title": "Unable to retrieve domain information"
            }
        },
        "add-a-subdomain": "Add a subdomain",
        "add-a-service": "Add a service",
        "add-alias": "Add alias",
        "add-an-alias": "Add an alias",
        "add-now": "Add it now!",
        "added-success": "Great! {domain} has been added. You can manage it right now.",
        "apply": {
            "additions": "no additions | {count} addition | {count} additions",
            "button": "Apply modifications",
            "deletions": "no deletions | {count} deletion | {count} deletions",
            "done": {
                "title": "Zone applied successfully!",
                "description": "!"
            },
            "nochange": "There is no changes to apply! Current zone is in sync with the server."
        },
        "attached-new": "New domain attached to happyDNS!",
        "create-new-key": "Create new {id} key",
        "discard": "Discard",
        "drop-alias": "Drop alias",
        "edit-target": "@:common.edit target",
        "give-explicit-name": "Give an explicit name in order to easily find this service.",
        "history": "History",
        "list": "List importable domains",
        "n-aliases": "{n} alias | {n} aliases",
        "please-fill-fields": "Please fill the following fields:",
        "removal": "Confirm Domain Removal",
        "save-modifications": "Save those modifications",
        "stop": "Stop managing this domain",
        "view": {
            "abstract": "Abstract zone",
            "cancel-title": "Keep my domain in happyDNS",
            "description": "Review the modifications that will be applied to {0}",
            "live": "Live records",
            "monitoring": "Monitoring",
            "summary": "Summary",
            "source": "Zone hosted on",
            "title": "View zone"
        },
        "views": {
            "grid": {
                "title": "Grid view (easiest)",
                "label": "Grid"
            },
            "list": {
                "title": "List view (fastest)",
                "label": "List"
            },
            "source-parameters": "Hosting parameters",
            "records": {
                "title": "List records (advanced)",
                "label": "Records"
            },
            "as": "View as:"
        },
        "alias-creation": "Add an alias pointing to {0}:",
        "alias-creation-sample": "This will create the alias:",
        "placeholder-new": "my.new.domain.",
        "form-new-subdomain": "Add a new subdomain under {0}:"
    },
    "email": {
        "address": "Email address",
        "instruction": {
            "check-inbox": "Please check your inbox in order to validate your e-mail address.",
            "new-confirmation": "If you need a new confirmation e-mail, just enter your address in the form below.",
            "validate-address": "In order to validate your e-mail address, please check your inbox, and follow the link contained in the message.",
            "validated": "Your new e-mail address is now validated!"
        },
        "send-again": "Re-send the confirmation e-mail",
        "send-recover": "Send me an e-mail to recover my account",
        "sent": "Confirmation e-mail sent!",
        "sent-recovery": "Password recovery email send!",
        "recover": "In order to recover your account, we'll send you an e-mail containing a link that will allow you to redefine your password"
    },
    "errors": {
        "404": {
            "title": "Page not found",
            "content": "The page you are look for was not found."
        },
        "account-delete": "An error occurs when trying to delete your account",
        "address": "Email address is required",
        "address-valid": "A valid email address is required",
        "domain-access": "An error occurs when trying to access domain's list.",
        "domain-attach": "An error occurs when attaching the domain to happyDNS",
        "domain-have": "It appears you don't have any domain name registered on this provider.",
        "domain-list": "This provider doesn't permit to list existing domains. Use the form below to add one.",
        "error": "Error",
        "login": "Login error",
        "logout": "Logout error",
        "occurs": "An error occurs when {when}!",
        "password": "Password is required",
        "password-change": "Unable to change your password account",
        "password-match": "Password and its confirmation doesn't match.",
        "password-weak": "Password needs to be stronger: at least 8 characters with numbers, low case and high case characters.",
        "recovery": "Password recovery problem",
        "resolve": "An error occurs when trying to resolve the domain.",
        "registration": "Registration problem",
        "rr-add": "An error occurs when trying to add RR to the zone:",
        "rr-delete": "An error occurs when trying to delete RR in the zone:",
        "source-delete": "Something went wrong during source deletion",
        "session": {
            "title": "Authentication timeout",
            "content": "Invalid session, your have been logged out: {err}. Please login again."
        },
        "settings-change": "Unable to change your settings",
        "account-no-auth": "You're using happyDNS without authentication. You cannot manage other account properties.",
        "domain-all-imported": "This account on {0} doesn't have any more domain to import."
    },
    "menu": {
        "my-domains": "My domains",
        "my-sources": "My domains' hosters",
        "dns-resolver": "DNS resolver",
        "my-account": "My account",
        "logout": "Logout",
        "signup": "Sign up",
        "signin": "Sign in",
        "quick-menu": "Quick Access"
    },
    "onboarding": {
        "add-one": "add a new one",
        "choose-configured": "choose between already configured providers or {0}:",
        "suggest-source": "choose your provider:",
        "use": "Use {happyDNS} as a remplacement interface to your usual domain name provider. It'll still rely on your provider's infrastructure, you'll just take benefit from our simple interface. As a first step, {first-step}",
        "no-sale": {
            "title": "I don't own any domain",
            "description": "{0} does not sell domain yet. To start using our interface, you need to buy a domain from one of our supported provider",
            "buy-advice": "We'll provide some guidance in a near future on how to easily buy a domain name. So stay tune and get in touch with us if you'll help us to build a comprehensive guide."
        },
        "own": "I already own a domain",
        "questions": {
            "hosting": {
                "q": "I don't want to rely on my domain name hosting provider anymore. Can I host my domain name on your infrastructure",
                "a": "We'll provide such feature in a near future, as it's on our manifest. We choose to focus first on spreading the word that domain names are accessibles to everyone through this sweet interface, before targeting privacy, censorship, …"
            },
            "secondary": {
                "q": "I've my own infrastructure, can I use {0} as secondary authoritative server?",
                "a": "We'll provide such feature in a near future, as soon as our name server infrastructure is on."
            }
        }
    },
    "password": {
        "change": "Change my password",
        "changed": "Password Successfully Changed",
        "confirm-new": "Confirm your new password",
        "confirmation": "Password confirmation",
        "enter": "Enter your current password",
        "enter-new": "Enter your new password",
        "fill": "In order to recover your account, please fill the following form, with a fresh password.",
        "forgotten": "Forgotten password?",
        "new": "New password",
        "redefine": "Redefine my password",
        "redefined": "Password redefined successfully!",
        "success": "You can now login with your new password.",
        "success-change": "Your account's password has been changed with success."
    },
    "service": {
        "add": "Add service",
        "already": "Already managed",
        "delete": "@:common.delete the service",
        "update": "Update the service",
        "form-new": "Add a new service to {0}"
    },
    "source": {
        "delete": "@:common.delete this @:source.kind",
        "find": "Can't find your @:source.kind here?",
        "name-your": "Name your @:source.kind",
        "source": "Domains living on {0}",
        "kind": "domain's hoster",
        "select-provider": "First, you need to select the provider hosting your domain:",
        "select-source": "Select the place where your domain {0} is hosted",
        "source-name": "Host's name",
        "source-type": "Hosting type",
        "title": "Your domains' hosters",
        "available-types": "Resources Types available",
        "import-domains": "Import all domains",
        "new-form": "New @:source.kind form",
        "update": "Update @:source.kind"
    },
    "upgrade": {
        "title": "An update is available!",
        "content": "A new version of happyDNS is already available. To enable it, please click here to refresh the page."
    },
    "wait": {
        "asking-domains": "Asking provider for the existing domains...",
        "exporting": "Please wait while we export your zone …",
        "formating": "Please wait while we format your zone …",
        "importing": "Please wait while we are importing your domain …",
        "loading": "Loading the domain …",
        "loading-account": "Loading your account …",
        "loading-record": "Loading records …",
        "retrieving-setting": "Retrieving host settings' form...",
        "updating": "Updating your domain name host",
        "validating": "Validating domain …",
        "wait": "Please wait",
        "retrieving-domains": "Retrieving your domains...",
        "retrieving-source": "Retrieving hosting information..."
    },
    "settings": {
        "fieldhint": {
            "title": "Display hint for form fields",
            "hide": "Hide",
            "tooltip": "Tooltip near field",
            "focused": "Under field when focused",
            "always": "Under field, always"
        },
        "language": "Language",
        "save": "Save settings",
        "title": "Settings",
        "success": "Continue to enjoy happyDNS.",
        "success-change": "Your settings has been saved."
    },
    "record": {
        "A": "IPv4 address",
        "AAAA": "IPv6 address",
        "Expire": "Expire",
        "Mbox": "Administration e-mail",
        "Minttl": "Negative cache TTL",
        "Mx": "Mail server",
        "Ns": "Name server",
        "Preference": "Preference",
        "Priority": "Priority",
        "Refresh": "Refresh",
        "Retry": "Retry",
        "Serial": "Serial",
        "Target": "Target",
        "Txt": "Data",
        "Weight": "Weight"
    },
    "resolver": {
        "advanced": "With custom settings?",
        "custom": "Custom resolver",
        "custom-description": "Enter the addresse (a domain name or an IP) of the resolver you want to use.",
        "domain-description": "Indicate the domain you search the records. For example, you can try {0}.",
        "field-description": "What kind of DNS record you want to see. For example: A is for IPv4, AAAA is for IPv6, ... {0}",
        "field-description-more-info": "More information here",
        "resolver-description": "This is the server we will ask for the information.",
        "ttl": "Remaining time in cache",
        "showDNSSEC": "Show DNSSEC records in answer (if any)"
    }
}